package com.hfad.myapplication;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
interface ProductDao {

    @Query("SELECT * FROM product WHERE id = :id")
     ProductEntity getProductById ( String id);

    @Query("SELECT * FROM product")
     List<ProductEntity> getProducts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     void insertProduct(ProductEntity food);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     void insertProduct(List<ProductEntity> food);

    @Delete
     void deleteProduct( ProductEntity food);

    @Query("DELETE FROM product")
     void deleteProducts();

    // TODO - нет необходимости в этом методе, т.к. OnConflictStrategy.REPLACE обновляет объект в базе сравнивая по id


}