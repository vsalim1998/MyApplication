package com.hfad.myapplication;

import java.util.List;

import retrofit2.Call;

public class DataManager {

    private Api api;

    public DataManager(Api api) {
        this.api = api;
    }

    public Call<List<Product>> getProduct() {
        return api.getProduct();
    }
}
