package com.hfad.myapplication;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    private String id;

    @SerializedName("date")
    private String date;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("new")
    private boolean newest;

    @SerializedName("image_app")
    private String image_app;

    @SerializedName("image")
    private String image;

    @SerializedName("price")
    private int price;

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isNewest() {
        return newest;
    }

    public String getImage_app() {
        return image_app;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }
}