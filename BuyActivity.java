package com.hfad.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hfad.myapplication.R.*;

public class BuyActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Database database;
    ProductDao productDao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( layout.buy_activity );
        database = App.getInstance ().getDatabase ();
        productDao = database.productDao ();
        recyclerView = findViewById ( id.recyclerView );
        if (productDao.getProducts ().isEmpty ()) {
            getProductsShow (true);
        }else
            getProductsShow ( false );
    }

    public void showProduct(List<Product> products) {
        LinearLayoutManager layoutManager = new LinearLayoutManager ( this );
        recyclerView.setLayoutManager ( layoutManager );

        MyAdapter adapter = new MyAdapter ( products, getApplicationContext (), productDao );
        recyclerView.setAdapter ( adapter );

    }

    public void onBackPressed(View view) {
        Intent intent = new Intent ( this, MainActivity.class );
        startActivity ( intent );
    }

    public void onSearchClick(View view) {
        Intent intent = new Intent ( this, SearchActivity.class );
        startActivity ( intent );
    }

    public void getProductsShow(final boolean databaseIsEmpty) {
        App.dataManager.getProduct ().enqueue ( new Callback<List<Product>> () {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (databaseIsEmpty) {
                    for (Product product :
                            response.body ()) {
                        ProductEntity productEntity = new ProductEntity ();
                        productEntity.id = product.getId ();
                        productEntity.amount = 0;
                        productDao.insertProduct ( productEntity );
                    }
                }
                showProduct ( response.body () );
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                showmasage ();
            }
        } );

    }


    private void showmasage() {
        Toast.makeText ( this, "response error", Toast.LENGTH_LONG ).show ();
    }
}
