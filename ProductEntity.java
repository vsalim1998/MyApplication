package com.hfad.myapplication;



import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

// TODO - Тебе необходимо хранить локально только id - товара и количество, т.к. данные в API могут измениться,
//  а ты будешь подставлять локальные данные и они будут неактуальными.

@Entity(tableName = "product")
public class ProductEntity implements Serializable {

    @PrimaryKey
    @NonNull
    public String id;

    @ColumnInfo(name = "amount")
    public int amount;
}