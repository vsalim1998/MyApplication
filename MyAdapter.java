package com.hfad.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;
    List<Product> products;
    ProductDao productDao;

    public MyAdapter(List<Product> products, Context context, ProductDao productDao) {
        this.products = products;
        this.context = context;
        this.productDao = productDao;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind ( products.get ( position ) );
    }

    @Override
    public int getItemCount() {
        return products.size ();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, description, price, amount;
        ImageView myImage;
        ImageButton minus, plus;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name );
            description = itemView.findViewById(R.id.description );
            price = itemView.findViewById ( R.id.price );
            myImage = itemView.findViewById(R.id.myImage );
            minus = itemView.findViewById ( R.id.min );
            plus = itemView.findViewById ( R.id.plus );
            amount = itemView.findViewById ( R.id.amount );
        }

        @SuppressLint("SetTextI18n")
        public void bind(final Product product) {
            name.setText ( product.getName () );
            description.setText ( product.getDescription ()  );
            price.setText (product.getPrice ()  + " ₽");
            Glide.with ( itemView.getContext () ).
                    load ( product.getImage () ).
                    into ( myImage );
            Log.v ("DATABASE PRODUCT", String.valueOf ( productDao.getProductById ( product.getId () ).amount ));

            if (productDao.getProductById ( product.getId () ).amount <= 0) {
                amount.setVisibility ( View.GONE );
                minus.setVisibility ( View.GONE );
            } else {
                amount.setText (String.valueOf (  productDao.getProductById ( product.getId () ).amount) );
                amount.setVisibility ( View.VISIBLE );
                minus.setVisibility ( View.VISIBLE );
            }
            minus.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick(View view) {
                    int i = Integer.parseInt ( amount.getText ().toString () );
                    ProductEntity productEntity = productDao.getProductById ( product.getId ()  );
                    productEntity.amount--;
                    productDao.insertProduct ( productEntity );
                    if (i > 1) {
                        amount.setText ( String.valueOf ( i - 1 ) );}
                    else if (i <= 0) {
                        amount.setText ( String.valueOf ( 0 ) );
                        minus.setVisibility ( View.GONE );
                        amount.setVisibility ( View.GONE );
                    }
                }
            } );

            plus.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick(View view) {
                    int i = Integer.parseInt ( amount.getText ().toString () );
                    minus.setVisibility ( View.VISIBLE );
                    amount.setVisibility ( View.VISIBLE );
                    amount.setText ( String.valueOf ( i + 1 ) );
                    ProductEntity productEntity = productDao.getProductById ( product.getId () );
                    productEntity.amount++;
                    productDao.insertProduct ( productEntity );
                }
            } );
        }
    }
}
