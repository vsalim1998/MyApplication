package com.hfad.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // TODO - Называй методы более понятнее, чтобы описывалась реализация (например: onCatalogClicked())
    public void onClick(View view) {
        Intent intent = new Intent(this, BuyActivity.class);
        startActivity(intent);
    }
}
