package com.hfad.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;



public interface Api {

    @GET("/api/v2/products?category=93&key=47be9031474183ea92958d5e255d888e47bdad44afd5d7b7201d0eb572be5278")
    Call<List<Product>> getProduct();

}
