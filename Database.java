package com.hfad.myapplication;


import androidx.room.RoomDatabase;

@androidx.room.Database (entities = {ProductEntity.class}, version = 5, exportSchema = false)
public abstract class Database extends RoomDatabase {
    public abstract ProductDao productDao();
}
