package com.hfad.myapplication;

import android.app.Application;

import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    public static DataManager dataManager;
    private static String BASE_URL = "https://peretz-group.ru";
    public static App instance;

    private Database database;

    @Override
    public void onCreate() {
        super.onCreate();
        getApi();
        instance = this;
        database = Room.databaseBuilder(this, Database.class, "database").allowMainThreadQueries ().fallbackToDestructiveMigration ()
                .build();
    }

    public static Api getApi() {


        Gson gson = new GsonBuilder ()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory( GsonConverterFactory.create(gson))
                .build();

        Api userApi = retrofit.create(Api.class);

        dataManager = new DataManager(userApi);

        return userApi;
    }
    public static App getInstance() {
        return instance;
    }

    public Database getDatabase() {
        return database;
    }
}
